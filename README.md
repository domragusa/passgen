# xkcdpassgen

A simple python script to generate easy to remember but still good passwords (see: https://xkcd.com/936/).

## Installation

Copy all the files in a nice directory (ex. ~/.local/lib/passgen/), make the script executable and finally put a link to the script in a directory included in your $PATH (ex. ~/.local/bin).

## Usage

Just call the script, you can specify dictionaries and length, ex:

```console
$ passgen.py -w 5 -d ita -d eng
supportsluminousportatilecriticiparents
```

will generate a password with 5 italian and/or english words.

For more info, take a look at:

```console
$ passgen.py -h
usage: xkcdpassgen [-h] [-s] [-w WORDS] [-l LENGTH] [-n NUMBER] [-f FILE]
                   [-d DICTIONARY]

Passphrase generator (see: https://xkcd.com/936/)

optional arguments:
  -h, --help            show this help message and exit
  -s, --space           Print passphrase(s) with spaces.
  -w WORDS, --words WORDS
                        Number of words. (default: 4)
  -l LENGTH, --length LENGTH
                        Minimum passphrase length.
  -n NUMBER, --number NUMBER
                        Number of of passphrases to generate. (default: 1)
  -f FILE, --file FILE  Load and use a dictionary from file.
  -d DICTIONARY, --dictionary DICTIONARY
                        Select dictionaries.

```
