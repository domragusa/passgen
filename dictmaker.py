#!/usr/bin/env python3

import sys, argparse, re, html, json, asyncio, aiohttp, aiofiles

async def worker(queue):
    result = []
    while not queue.empty():
        src = await queue.get()
        try:
            if re.match('http(s)?://', src):
                async with aiohttp.request('GET', src) as conn:
                    data = await conn.read()
            else:
                async with aiofiles.open(src, 'r') as fh:
                    data = await fh.read()
        except Exception as error:
            data = None
            print(f'Failed to get: {src}\nError: {error}', file=sys.stderr)
        if data is not None: result.append([src, data])
        queue.task_done()
        
    return result

def fetch_all(urls):
    asyncio.set_event_loop(asyncio.new_event_loop())
    loop = asyncio.get_event_loop()
    
    queue = asyncio.Queue(loop=loop)
    for x in urls: queue.put_nowait(x)
    
    tasks = [worker(queue) for _ in range(6)]
    data = loop.run_until_complete(asyncio.gather(*tasks))
    loop.close()
    
    return {url: text for url, text in sum(data,[])}

def extract_words(source, *, min_len=0):
    for t in ['head', 'a', 'script', 'style', 'object', 'math', 'svg']:
        source = re.sub(f'<{t}[^<{t}].+?</{t}>', ' ', source, flags=re.DOTALL)
    source = re.sub('<!\\-\\-[^<!\\-\\-](.+?)\\-\\->', ' ', source, flags=re.DOTALL)
    source = re.sub('<[^<]+?>', ' ', source, flags=re.DOTALL)
    source = html.unescape(source)
    
    words = re.split('\W+', source)
    words = [w.lower() for w in words if w.isalpha() and w.islower()]
    words = [w for w in words if len(w)>=min_len]
    return words

def make_dictionary(sources):
    dictionary = []
    for src in sources:
        dictionary += extract_words(src, min_len=args.length)
    return sorted(list(set(dictionary)))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='dictmaker',
        description='Dictionary maker for xkcdpassgen.')
    parser.add_argument('-l', '--length', default=3,
        help='Minimum word length.')
    parser.add_argument('-i', '--input', action='append', default=[], required=True,
        help='Input resource (file or url).')
    parser.add_argument('-o', '--output',
        help='Output file. (default: stdin)')
    args = parser.parse_args()
    
    dictionary = make_dictionary(fetch_all(args.input).values())
    out = '\n'.join(dictionary)
    if not args.output:
        print(out)
    else:
        with open(args.output, 'w') as fh:
            fh.write(out)

