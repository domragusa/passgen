#!/usr/bin/env python3

import random, os, argparse

rand = random.SystemRandom()

class password_generator:
    def __init__(self):
        self.dicts = {}
    
    def add_dictionary(self, label, *, path=None, wordlist=None):
        #don't reuse label!
        if path is None and wordlist is None:
            raise Exception('invalid dictionary')
        self.dicts[label] = {'path': path, 'list': wordlist}
    
    def load_dictionaries(self, dicts):
        def lazy_load(label):
            cur = self.dicts[label]
            if cur['list'] is None:
                with open(cur['path'], 'r') as fh:
                    cur['list'] = [l.strip() for l in fh.readlines()]
            return cur['list']
        wordlist = sum([lazy_load(label) for label in dicts], [])
        return list(set(wordlist))
    
    def get_password(self, num_words, *, dicts=None, at_least=0):
        if not dicts: dicts = self.dicts.keys()
        wordlist = self.load_dictionaries(dicts)
        for i in range(10): # max number of tries
            password = rand.sample(wordlist, num_words)
            cur_len = len(''.join(password))
            if cur_len >= at_least:
                return password
            if i > 5:       # increase number of words after 10 attempts
                num_words = int(at_least/(cur_len/num_words))
        raise Exception(f'failed to compose wanted password, num_words={num_words}, dicts={dicts}, at_least={at_least}')

def default_dictionaries():
    base_dir = os.path.dirname(os.path.realpath(__file__))
    dict_dir = f'{base_dir}/dictionaries'
    dicts = {}
    for name in os.listdir(dict_dir):
        path = f'{dict_dir}/{name}'
        if os.path.isfile(path) and name.endswith('.txt'):
            label = name.rsplit('.', 1)[0]
            dicts[label] = path
    return dicts

def default_parser():
    def pos_int(x):
        value = float(x)
        if not value.is_integer() or value <= 0:
            raise argparse.ArgumentTypeError(f'invalid positive int value: {x}')
        return int(x)
    parser = argparse.ArgumentParser(prog='xkcdpassgen',
        description='Passphrase generator (see: https://xkcd.com/936/)')
    parser.add_argument('-s', '--space', action='store_true',
        help='Print passphrase(s) with spaces.')
    parser.add_argument('-w','--words', type=pos_int, default=4,
        help='Number of words. (default: %(default)s)')
    parser.add_argument('-l', '--length', type=pos_int, default=1,
        help='Minimum passphrase length.')
    parser.add_argument('-n','--number', type=pos_int, default=1,
        help='Number of of passphrases to generate. (default: %(default)s)')
    parser.add_argument('-f', '--file', action='append', default=[],
        help='Load and use a dictionary from file.')
    parser.add_argument('-d', '--dictionary', action='append', default=[],
        help='Select dictionaries.')
    return parser

if __name__ == '__main__':
    parser = default_parser()
    args = parser.parse_args()
    
    pg = password_generator()
    for label, path in default_dictionaries().items():
        pg.add_dictionary(label, path=path)
    
    for i, path in enumerate(args.file):
        pg.add_dictionary(f':{i}', path=path)
        args.dictionary.append(f':{i}')
    
    for _ in range(args.number):
        passw = pg.get_password(args.words, dicts=args.dictionary, at_least=args.length)
        print((' ' if args.space else '').join(passw))
    
